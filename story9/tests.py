from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from .views import *
from .models import *
import time
import random
import string
from django.http import *


# Create your tests here.
class StoryUnitTest (TestCase):
    def test_homepage_url_template_and_function(self):
        response = Client().get('')
        found = resolve('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertEqual(found.func, index)
    
    def test_model(self):
        letters = string.ascii_letters
        random_book_id = ''.join(random.choice(letters) for i in range(10))
        random_book_id_2 = ''.join(random.choice(letters) for i in range(10))
        random_like_count = random.randint(1,10)
        title = 'This is a book'
        authors = None

        Books.objects.create(
            book_id = random_book_id_2,
            image_link = '',
            title = title,
            authors = authors,
            like = random_like_count,
        )

        data = Books.objects

        self.assertEqual(data.all().count(),1)
        self.assertEqual(str(data.get(title=title)), title)

    def test_model_incomplete_field(self):
        book_test = {"id": "aoToOxruBiQC",  
                        "volumeInfo": {"title": "IPA - Asyik, mudah, dan menyenangkan 3A - untul Sekolah Dasar - Kelas 3 Semester 1",
                            "imageLinks": {
                                "smallThumbnail": "http://books.google.com/books/content?id=aoToOxruBiQC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                                "thumbnail": "http://books.google.com/books/content?id=aoToOxruBiQC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
                            },
                        }
                    }
        random_like_count = random.randint(1,10)

        try:
            Books.objects.create(
                book_id = book_test['id'],
                image_link = book_test['volumeInfo']['imageLinks']['thumbnail'],
                title = book_test['volumeInfo']['title'],
                authors = book_test['volumeInfo']['authors'],
                like = random_like_count,
            )
        except:
            data = Books.objects
        data = Books.objects
        self.assertEqual(data.all().count(),0)




class StoryFunctionalTest(LiveServerTestCase):
    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
    
    def tearDown(self) :
        self.driver.quit()
        super().tearDown()

    def test_when_searching_for_a_book_then_it_will_return_a_table_of_books(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        ai_book = 'Artificial Intelligence a Modern Approach'
        for i in ai_book:
            self.driver.find_element_by_id('search').send_keys(i)
            time.sleep(0.1)

        time.sleep(2)

        response_content = self.driver.page_source
        self.assertIn('Artificial Intelligence', response_content)
        self.assertIn('Stuart Russell', response_content)
        self.assertIn('Peter Norvig', response_content)
    
    def test_searching_for_a_book_with_undefined_author(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        ai_book = 'ipa : - kelas x'
        for i in ai_book:
            self.driver.find_element_by_id('search').send_keys(i)
            time.sleep(0.1)

        time.sleep(2)

        response_content = self.driver.page_source
        self.assertIn('IPA : - Kelas X', response_content)
        self.assertIn('undefined', response_content)

        self.driver.find_element_by_id('unlikeBtn2').click()

        response_content = self.driver.page_source
        self.assertIn('0', response_content)

    def test_when_like_button_is_click_then_it_increment_the_number_of_likes(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        ai_book = 'Artificial Intelligence a Modern Approach'
        for i in ai_book:
            self.driver.find_element_by_id('search').send_keys(i)
            time.sleep(0.1)

        time.sleep(2)

        response_content = self.driver.page_source
        self.assertIn('Like', response_content)
        self.assertIn('Unlike', response_content)

        self.driver.find_element_by_id('likeBtn0').click()

        response_content = self.driver.page_source
        self.assertIn('1', response_content)
    
    def test_when_unlike_button_is_click_then_it_decrement_the_number_of_likes(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        ai_book = 'Artificial Intelligence a Modern Approach'
        for i in ai_book:
            self.driver.find_element_by_id('search').send_keys(i)
            time.sleep(0.1)
        
        time.sleep(2)

        response_content = self.driver.page_source
        self.assertIn('Like', response_content)
        self.assertIn('Unlike', response_content)

        self.driver.find_element_by_id('likeBtn0').click()
        self.driver.find_element_by_id('unlikeBtn0').click()

        response_content = self.driver.page_source
        self.assertIn('0', response_content)
    
    def test_when_top_5_books_button_is_click_then_it_will_show_top_5_books_with_the_most_likes(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        ai_book = 'Artificial Intelligence a Modern Approach'
        for i in ai_book:
            self.driver.find_element_by_id('search').send_keys(i)
            time.sleep(0.1)
        
        time.sleep(2)

        for i in range(10):
            self.driver.find_element_by_id('likeBtn0').click()

        self.driver.find_element_by_id('top5books').click()

        response_content = self.driver.page_source

        self.assertIn('Artificial Intelligence', response_content)
        self.assertIn('Stuart Russell', response_content)
        self.assertIn('Peter Norvig', response_content)
        self.assertIn('10', response_content)