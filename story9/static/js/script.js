var booksData = [];

$(document).ready(function() {
    $('#book-form').on("keyup",function (event) {
        event.preventDefault();
        const q = $('#search').val();
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var data = JSON.parse(this.responseText);
                var result = data.items;
                booksData = result;
                $("#content").html("");
                for(var i = 0; i < result.length; i++) {
                    $("#content").append(
                        "<tr>" +
                            "<td>" + "<img src= '"+ result[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                            "<td class='table-heading align-middle text-center'>" + result[i].volumeInfo.title + "</td>" +
                            "<td class='table-heading align-middle text-center'>" + result[i].volumeInfo.authors + "</td>" +
                            "<td class='table-heading align-middle text-center'>" + 
                                "<button class='btn' style='width:75px; margin-right:10px;' type='button' onclick='likebooks("+ i +")' id='likeBtn" + i + "'>Like</button>" + 
                                "<button class='btn' style='width:75px;' type='button' onclick='unlikebooks("+ i +")' id='unlikeBtn" + i + "'>Unlike</button>" + 
                                "<p id='like" + i + "' data-value='0'></p></td>" +
                        "</tr>"
                    )
                }
            }
        };
        xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + q, true);
        xhttp.send();
    });
});

function likebooks(i) {
    var sendData = JSON.stringify(booksData[i]);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var like = JSON.parse(this.responseText).likeCount;
            $('#like'+i).html(like);
        }
    }
    xhttp.open("POST", "/api/likebooks/");
    xhttp.send(sendData);
}

function unlikebooks(i) {
    var sendData = JSON.stringify(booksData[i]);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var like = JSON.parse(this.responseText).likeCount;
            $('#like'+i).html(like);
        }
    }
    xhttp.open("POST", "/api/unlikebooks/");
    xhttp.send(sendData);
}

function topbooks() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var books = JSON.parse(this.responseText).books;
            $('.modal-body').html('');
            for (var i = 0; i < 5; i++) {
                $(".modal-body").append(
                    "<p style='font-size: 25px;'>" + books[i]['title'] + "</p>" +
                    "<p style='color: #AAA;'>Author(s): " + removeBrackets(books[i]['authors']) + "</p>" +
                    "<p style='color: #52c234;'>" + books[i]['like'] + " likes</p>"
                )
                if(i != 4) {
                    $(".modal-body").append(
                        "<br>"
                    )
                }
            }
        }
    }
    xhttp.open("GET", "/api/topbooks/", true);
    xhttp.send();
}

function removeBrackets(authors_with_brackets) {
    var authors = "";
    for (var i = 0; i < authors_with_brackets.length; i++) {
        if(!(authors_with_brackets.charAt(i) == "'") && !(authors_with_brackets.charAt(i) == "[") && !(authors_with_brackets.charAt(i) == "]")) {
            authors += authors_with_brackets.charAt(i);
        }
    }
    return authors;
}
