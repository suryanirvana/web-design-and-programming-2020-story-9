from django.urls import path
from .views import *

app_name='story9'
urlpatterns = [
    path('', index, name='index'),
    path('api/likebooks/', like_books, name='like_books'),
    path('api/unlikebooks/', unlike_books, name='unlike_books'),
    path('api/topbooks/', top_books, name='top_books'),
]