import json
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import *

# Create your views here.
def index(request):
    return render(request, 'index.html')

@csrf_exempt
def like_books(request):
    data = json.loads(request.body)
    try:
        book = Books.objects.get(book_id= data['id'])
        book.like += 1
        book.save()
    except:
        book = Books.objects.create(
            book_id = data['id'],
            image_link = data['volumeInfo']['imageLinks']['thumbnail'],
            title = data['volumeInfo']['title'],
            authors = 'undefined' if 'authors' not in data['volumeInfo'] else data['volumeInfo']['authors'],
            like = 1
        )
    return JsonResponse({
        'likeCount' : book.like
    })

@csrf_exempt
def unlike_books(request):
    data = json.loads(request.body)
    try:
        book = Books.objects.get(book_id= data['id'])
        if book.like > 0:
            book.like -= 1
        book.save()
    except:
        book = Books.objects.create(
            book_id = data['id'],
            image_link = data['volumeInfo']['imageLinks']['thumbnail'],
            title = data['volumeInfo']['title'],
            authors = 'undefined' if 'authors' not in data['volumeInfo'] else data['volumeInfo']['authors'],
        )
    return JsonResponse({
        'likeCount' : book.like
    })

def top_books(request):
    books = Books.objects.order_by('-like')

    book_list = []
    for book in books:
        book_list.append({
            'image_link': book.image_link,
            'title': book.title,
            'authors': book.authors,
            'like': book.like,
        })

    return JsonResponse({
        'books' : book_list
    })